-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.11-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para covidunivalle
CREATE DATABASE IF NOT EXISTS `covidunivalle` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `covidunivalle`;

-- Volcando estructura para tabla covidunivalle.formulario
CREATE TABLE IF NOT EXISTS `formulario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `enfermedad` varchar(50) NOT NULL DEFAULT '',
  `sitios_frecuentados` varchar(200) NOT NULL DEFAULT '',
  `cantidad_menores` int(11) NOT NULL,
  `cantidad_mayores` int(11) NOT NULL,
  `numero_documento_usuario` bigint(10) unsigned NOT NULL DEFAULT 0,
  `numero_documento_vigilante` bigint(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_numero_documento_usuario` (`numero_documento_usuario`),
  KEY `fk_numero_documento_vigilante` (`numero_documento_vigilante`),
  CONSTRAINT `fk_numero_documento_usuario` FOREIGN KEY (`numero_documento_usuario`) REFERENCES `usuario` (`numero_documento`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_numero_documento_vigilante` FOREIGN KEY (`numero_documento_vigilante`) REFERENCES `vigilante` (`numero_documento`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla covidunivalle.formulario: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `formulario` DISABLE KEYS */;
/*!40000 ALTER TABLE `formulario` ENABLE KEYS */;

-- Volcando estructura para tabla covidunivalle.ingreso
CREATE TABLE IF NOT EXISTS `ingreso` (
  `id_ingreso` int(11) NOT NULL AUTO_INCREMENT,
  `hora_ingreso` time NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `temperatura` float DEFAULT NULL,
  `numero_documento_usuario` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_ingreso`),
  KEY `FK1_numero_documento` (`numero_documento_usuario`),
  CONSTRAINT `FK1_numero_documento` FOREIGN KEY (`numero_documento_usuario`) REFERENCES `usuario` (`numero_documento`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla covidunivalle.ingreso: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ingreso` DISABLE KEYS */;
/*!40000 ALTER TABLE `ingreso` ENABLE KEYS */;

-- Volcando estructura para tabla covidunivalle.programa
CREATE TABLE IF NOT EXISTS `programa` (
  `codigo` int(10) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla covidunivalle.programa: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `programa` DISABLE KEYS */;
INSERT INTO `programa` (`codigo`, `nombre`) VALUES
	(2230, 'Psicología'),
	(2711, 'Desarollo de Software'),
	(2712, 'Tecnología electrónica'),
	(5520, 'Ingeniería industrial');
/*!40000 ALTER TABLE `programa` ENABLE KEYS */;

-- Volcando estructura para tabla covidunivalle.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `numero_documento` bigint(10) unsigned NOT NULL,
  `tipo_documento` varchar(50) NOT NULL DEFAULT '',
  `nombre` varchar(50) NOT NULL DEFAULT '',
  `apellido` varchar(50) NOT NULL DEFAULT '',
  `cargo` varchar(50) NOT NULL DEFAULT '',
  `telefono` bigint(20) NOT NULL DEFAULT 0,
  `direccion` varchar(50) NOT NULL DEFAULT '',
  `ciudad` varchar(50) NOT NULL DEFAULT '',
  `codigo_programa` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`numero_documento`),
  KEY `FK1_codigo_programa` (`codigo_programa`),
  CONSTRAINT `FK1_codigo_programa` FOREIGN KEY (`codigo_programa`) REFERENCES `programa` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla covidunivalle.usuario: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`numero_documento`, `tipo_documento`, `nombre`, `apellido`, `cargo`, `telefono`, `direccion`, `ciudad`, `codigo_programa`) VALUES
	(1115093055, 'cedula', 'Daniel', 'Kure', 'Estudiante', 2147483647, 'Calle 3s 4.40', 'Buga', 2711);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

-- Volcando estructura para tabla covidunivalle.vigilante
CREATE TABLE IF NOT EXISTS `vigilante` (
  `numero_documento` bigint(10) unsigned NOT NULL DEFAULT 0,
  `tipo_documento` varchar(50) NOT NULL,
  `contrasena` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `telefono` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`numero_documento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla covidunivalle.vigilante: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `vigilante` DISABLE KEYS */;
INSERT INTO `vigilante` (`numero_documento`, `tipo_documento`, `contrasena`, `nombre`, `apellido`, `direccion`, `telefono`) VALUES
	(1113040601, 'cedula', 'carlos1', 'Carlos', 'Vergara', 'Calle 3 15-22', 3174445689),
	(1113040602, 'cedula', 'pabloelpablo', 'Pablo', 'Paisa', 'Carrera 6 5-14', 3214567891);
/*!40000 ALTER TABLE `vigilante` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
