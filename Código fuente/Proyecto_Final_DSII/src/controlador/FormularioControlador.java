/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.FileWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Formulario;
import modelo.RegistroFormulario;
import modelo.Usuario;
import modelo.Vigilante;
import persistencia.DAOFactory;
import persistencia.EntityManagerHelper;

/**
 *
 * @author Windows
 */
public class FormularioControlador {

    public void enviar(RegistroFormulario rfm) throws Exception {

        if (rfm == null) {
            JOptionPane.showMessageDialog(null,"Por favor ingrese datos");
        }

        if ("".equals(rfm.getCantidadMayores())) {
            JOptionPane.showMessageDialog(null,"Llene el campo cantidad de mayores.");
        }

        if ("".equals(rfm.getCantidadMenores())) {
            JOptionPane.showMessageDialog(null,"Llene el campo cantidad de menores.");
        }

        if ("".equals(rfm.getEnfermedad())) {
             JOptionPane.showMessageDialog(null,"Llene el campo cantidad de enfermedades.");
        }

        if ("".equals(rfm.getSitiosFrecuentados())) {
             JOptionPane.showMessageDialog(null,"Llene el campo cantidad de sitios frecuentados.");
        }

        try {
            Formulario fm = new Formulario();

            fm.setEnfermedad(rfm.getEnfermedad());
            fm.setSitiosFrecuentados(rfm.getSitiosFrecuentados());
            fm.setCantidadMayores(rfm.getCantidadMayores());
            fm.setCantidadMenores(rfm.getCantidadMenores());
            Usuario usu = DAOFactory.getUsuarioDAO().consultarPorDocumento(rfm.getNumDocUsuario());
            Vigilante vig = DAOFactory.getVigilantesDAO().consultarCampo(rfm.getNumDocVigilante());
            fm.setNumeroDocumentoUsuario(usu);
            fm.setNumeroDocumentoVigilante(vig);

            EntityManagerHelper.beginTransaction();
            DAOFactory.getFormularioDAO().insertar(fm);
            EntityManagerHelper.commit();
            EntityManagerHelper.closeEntityManager();
            try (FileWriter fw = new FileWriter("reportes.txt", true)) {
                fw.append("El usuario con número de documento " + rfm.getNumDocUsuario() + " presentó una anormalidad en su temperatura corporal al momento de ingresar a las instalaciones. El vigilante con cédula " + rfm.getNumDocVigilante() + " registró a este usuario.\n");
                fw.close();
            } catch (Exception x) {
            }
        } catch (Exception e) {
            Logger.getLogger(IngresoControlador.class.getName()).log(Level.SEVERE, null, e);
        }



    }
}
