/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import modelo.Ingreso;
import modelo.RegistroUsuario;
import modelo.RegistroIngreso;
import modelo.Usuario;
import persistencia.DAOFactory;
import persistencia.EntityManagerHelper;
import vista.Ingreso_Vista;
import vista.Novedades;



/**
 *
 * @author Windows
 */
public class IngresoControlador {
    
    public void enviar(RegistroIngreso rgi) throws Exception {
        

        if (rgi == null) {
             JOptionPane.showMessageDialog(null,"Por favor ingrese datos");
        }

        if ("".equals(rgi.getTemperatura())) {
             JOptionPane.showMessageDialog(null,"Llene el campo de temperatura");
        }

        if ("".equals(rgi.getFecha())) {
             JOptionPane.showMessageDialog(null,"Llene el campo Fecha");
        }

        if ("".equals(rgi.getHora())) {
             JOptionPane.showMessageDialog(null,"Llene el campo de Hora");
        }
        
        if (!fechaCheck(rgi.getFecha())){
             JOptionPane.showMessageDialog(null,"Error en formato fecha. Recuerde, el formato es: YYYY-MM-DD");
        }
        
        if (!horaCheck(rgi.getHora())){
             JOptionPane.showMessageDialog(null,"Error en formato hora. Recuerde, el formato es: HH:MM");
        }
        

        
        try {
            Ingreso ing = new Ingreso();
            SimpleDateFormat formatters = new SimpleDateFormat("HH:mm");
            Date time = formatters.parse(rgi.getHora());
            ing.setHoraIngreso(time);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = formatter.parse(rgi.getFecha());
            ing.setFechaIngreso(date);
            ing.setTemperatura(rgi.getTemperatura());
            Usuario usu = DAOFactory.getUsuarioDAO().consultarPorDocumento(rgi.getNumDocumento());
            ing.setNumeroDocumentoUsuario(usu);
            EntityManagerHelper.beginTransaction();
            DAOFactory.getIngresoDAO().insertar(ing);
            EntityManagerHelper.commit();
            EntityManagerHelper.closeEntityManager();
            
        } catch (Exception e) {
            Logger.getLogger(IngresoControlador.class.getName()).log(Level.SEVERE, null, e);
        }
        
        if(rgi.getTemperatura() >= 37){
            Novedades nv = new Novedades(null,true,rgi.getNumDocumento(),rgi.getNumDocVig());
            nv.setVisible(true);
        }

       

    }
    
        public boolean fechaCheck(String fecha) {
        Pattern pattern = Pattern.compile("[0-9]{4}-[0-9]{2}-[0-9]{2}");
        Matcher mat = pattern.matcher(fecha);

        if (mat.matches()) {
            boolean valid=false;
            String fechax[] = fecha.split("-");
            for(int i=0;i<fechax.length;i++){
                if(Integer.parseInt(fechax[i])==00){
                    valid=false;
                    break;
                }else{
                    valid=true;
                }
            }
            return valid;
            
            
        } else {
            return false;
        }
    

    }
    
    public boolean horaCheck(String hora) {
        Pattern pattern = Pattern.compile("[0-9]{2}:[0-9]{2}");
        Matcher mat = pattern.matcher(hora);

        if (mat.matches()) {
            boolean valid=false;
            String[] horax = hora.split(":");
            
            if( Integer.parseInt(horax[0])>=24  ||  Integer.parseInt(horax[1]) >=60) {
                valid =false;
            }else{
                valid = true;
            }
            return valid;
            
        } else {
            return false;
        }
    

    }


    }
    
    

