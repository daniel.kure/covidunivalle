/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Programa;
import modelo.RegistroUsuario;
import modelo.Usuario;
import persistencia.DAOFactory;
import persistencia.EntityManagerHelper;
import vista.Informacion_Usuario;
import vista.Ingreso_Vista;

/**
 *
 * @author Windows
 */
public class RegistroControlador {

    public void enviar(RegistroUsuario rg) throws Exception {

        if (rg == null) {
            JOptionPane.showMessageDialog(null, "Por favor ingrese datos");
        }

        if ("".equals(rg.getNombre())) {
            JOptionPane.showMessageDialog(null, "Llene el campo de nombre");
        }

        if ("".equals(rg.getApellido())) {
            JOptionPane.showMessageDialog(null, "Llene el campo de apellido");
        }

        if ("".equals(rg.getTipoDocumento())) {
            JOptionPane.showMessageDialog(null, "Llene el campo de tipo documento");
        }

        if ("".equals(rg.getCargo())) {
            JOptionPane.showMessageDialog(null, "Llene el campo de cargo");
        }

        if ("".equals(rg.getPrograma())) {
            JOptionPane.showMessageDialog(null, "Llene el campo de programa");
        }

        if ("".equals(rg.getCiudad())) {
            JOptionPane.showMessageDialog(null, "Llene el campo de ciudad");
        }

        if ("".equals(rg.getDireccion())) {
            JOptionPane.showMessageDialog(null, "Llene el campo de direccion");
        }

        if (!buscarPrograma(rg)) {
            JOptionPane.showMessageDialog(null, "No existe ese programa en la base de datos");
        }

        try {
            Usuario us = new Usuario();
            us.setNumeroDocumento(rg.getNumDocumento());
            us.setNombre(rg.getNombre());
            us.setApellido(rg.getApellido());
            us.setTipoDocumento(rg.getTipoDocumento());
            us.setCargo(rg.getCargo());
            Programa pg = DAOFactory.getProgramaDAO().consultarPorCodigo(rg.getPrograma());
            us.setCodigoPrograma(pg);
            us.setTelefono(rg.getTelefono());
            us.setCiudad(rg.getCiudad());
            us.setDireccion(rg.getDireccion());

            EntityManagerHelper.beginTransaction();

            DAOFactory.getUsuarioDAO().insertar(us);
            EntityManagerHelper.commit();
            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            Logger.getLogger(RegistroControlador.class.getName()).log(Level.SEVERE, null, e);
        }

        Ingreso_Vista iv = new Ingreso_Vista(null, true, rg.getNumDocumento(), rg.getNumDocVig());
        iv.setVisible(true);

    }

    public boolean encontradoBD(RegistroUsuario rg) throws Exception {
        if (rg == null) {
             JOptionPane.showMessageDialog(null,"Vacio");
        }

        if (rg.getNumDocumento() == 0) {
             JOptionPane.showMessageDialog(null,"El número de documento no puede ser 0");
        }

        boolean encontro = false;

        if (DAOFactory.getUsuarioDAO().consultarPorDocumento(rg.getNumDocumento()) != null) {
            encontro = true;
            Ingreso_Vista iv = new Ingreso_Vista(null, true, rg.getNumDocumento(), rg.getNumDocVig());
            iv.setVisible(true);
        }

        return encontro;
    }

    /* public String[] llevarDatos(RegistroUsuario rg) throws Exception {
        String datos[] = new String[7];

        try {
            java.util.List<Usuario> usuarios = DAOFactory.getUsuarioDAO().consultar();
            for (Usuario usuariox : usuarios) {
                if (usuariox.getNumeroDocumento().equals(rg.getNumDocumento())) {
                    datos[0] = usuariox.getNombre();
                    datos[1] = usuariox.getApellido();
                    datos[2] = usuariox.getTipoDocumento();
                    datos[3] = usuariox.getCargo();
                    datos[4] = "algo";
                    datos[5] = usuariox.getCiudad();
                    datos[6] = usuariox.getDireccion();

                }
            }

        } catch (Exception e) {

        }

        return datos;
    }
     */
    public boolean buscarPrograma(RegistroUsuario rg) throws Exception {
        boolean encontro = false;

        if (DAOFactory.getProgramaDAO().consultarPorCodigo(rg.getPrograma()) != null) {
            encontro = true;
        }

        return encontro;
    }
}
