/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Login;
import modelo.Vigilante;
import persistencia.DAOFactory;
import vista.Informacion_Usuario;

/**
 *
 * @author Windows
 */
public class LoginControlador {

    public void enviar (Login login) throws Exception{
        
        if(login == null){
            JOptionPane.showMessageDialog(null,"Vacio");
        }
        
        if("".equals(login.getUsuario())){
            JOptionPane.showMessageDialog (null,"Llene el campo de usuario");
        }
        
        if("".equals(login.getContrasena())){
            JOptionPane.showMessageDialog (null,"Llene el campo de contraseña");
        }
        
        if(DAOFactory.getVigilantesDAO().consultarCampo(login.getUsuario())== null){
            JOptionPane.showMessageDialog(null,"El usuario no está registrado en la base de datos");
        }
        boolean encontro = false;
        try {
            java.util.List<Vigilante> vigilantes = DAOFactory.getVigilantesDAO().consultar();
            for (Vigilante vigilantex : vigilantes) {
                      if(vigilantex.getNumeroDocumento().equals(login.getUsuario())  && vigilantex.getContrasena().equals(login.getContrasena()) ){
                          encontro=true;
                          Informacion_Usuario iu = new Informacion_Usuario(null,true,login.getUsuario());
                          iu.setVisible(true);
                      }             
            }
            
        } catch (Exception e) {
            
        }
        
        if(encontro==false){
            JOptionPane.showMessageDialog (null,"El usuario y la contraseña no coinciden");
        }
        
        
        
    
    }
    
}
