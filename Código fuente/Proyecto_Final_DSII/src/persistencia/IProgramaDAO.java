/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import modelo.Programa;
import modelo.Usuario;

/**
 *
 * @author Windows
 */
public interface IProgramaDAO {
    public Programa consultarPorCodigo(int numCodigo) throws Exception;
}
