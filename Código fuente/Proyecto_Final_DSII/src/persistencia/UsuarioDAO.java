/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.util.List;
import javax.persistence.Query;
import modelo.Usuario;

/**
 *
 * @author Windows
 */
public class UsuarioDAO implements IUsuarioDAO{
    public void insertar(Usuario us) throws Exception {
        try {
            EntityManagerHelper.getEntityManager().persist(us);
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
    public Usuario consultarPorDocumento(Long numDocumento) throws Exception {
        try {
            return EntityManagerHelper.getEntityManager().find(Usuario.class, numDocumento);
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
    public List<Usuario> consultar() throws Exception {
        try {
            Query query = EntityManagerHelper.getEntityManager().createNamedQuery("Usuario.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
}
