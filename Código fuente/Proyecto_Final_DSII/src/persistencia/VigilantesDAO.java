/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.util.List;
import javax.persistence.Query;
import modelo.Vigilante;

/**
 *
 * @author Windows
 */
public class VigilantesDAO implements IVigilantesDAO {

    public Vigilante consultarCampo(Long cedula) {
        try {
            return EntityManagerHelper.getEntityManager().find(Vigilante.class, cedula);
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
    public List<Vigilante> consultar() throws Exception {
        try {
            Query query = EntityManagerHelper.getEntityManager().createNamedQuery("Vigilante.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
   
    
}
