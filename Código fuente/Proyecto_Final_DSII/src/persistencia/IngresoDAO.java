/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.util.List;
import javax.persistence.Query;
import modelo.Ingreso;

/**
 *
 * @author Windows
 */
public class IngresoDAO implements IIngresoDAO{

    public void insertar(Ingreso ing) throws Exception {
        try {
            EntityManagerHelper.getEntityManager().persist(ing);
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
    public Ingreso consultarPorDocumento(Long numDocumento) throws Exception {
        try {
            return EntityManagerHelper.getEntityManager().find(Ingreso.class, numDocumento);
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
    public List<Ingreso> consultar() throws Exception {
        try {
            Query query = EntityManagerHelper.getEntityManager().createNamedQuery("Ingreso.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
}
