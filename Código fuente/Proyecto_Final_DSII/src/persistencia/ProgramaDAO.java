/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import modelo.Programa;

/**
 *
 * @author Windows
 */
public class ProgramaDAO implements IProgramaDAO {
    
    public Programa consultarPorCodigo(int numCodigo) throws Exception {
        try {
            return EntityManagerHelper.getEntityManager().find(Programa.class, numCodigo);
        } catch (RuntimeException e) {
            throw e;
        }
    }
}
