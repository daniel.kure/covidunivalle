/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.util.List;
import modelo.Ingreso;


/**
 *
 * @author Windows
 */
public interface IIngresoDAO {
    public void insertar(Ingreso ingreso) throws Exception;
    public Ingreso consultarPorDocumento(Long numDocumento) throws Exception;
    public List<Ingreso> consultar() throws Exception; 
}
