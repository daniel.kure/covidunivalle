/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.util.List;
import modelo.Vigilante;

/**
 *
 * @author Windows
 */
public interface IVigilantesDAO {
    public Vigilante consultarCampo(Long cedula);
    public List<Vigilante> consultar() throws Exception;
}
