/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

/**
 *
 * @author Windows
 */
public class DAOFactory {
    
    private static IVigilantesDAO vigilantesDAO = new VigilantesDAO();
    private static IUsuarioDAO usuarioDAO = new UsuarioDAO();
    private static IProgramaDAO programaDAO = new ProgramaDAO();
    private static IIngresoDAO ingresoDAO = new  IngresoDAO();
    private static IFormularioDAO formularioDAO = new FormularioDAO();
    
    public static IVigilantesDAO getVigilantesDAO(){
        return vigilantesDAO;
    }
    
    public static IUsuarioDAO getUsuarioDAO(){
        return usuarioDAO;
    }
    
    public static IProgramaDAO getProgramaDAO(){
        return programaDAO;
    }

    public static IIngresoDAO getIngresoDAO() {
        return ingresoDAO;
    }
    
    public static IFormularioDAO getFormularioDAO(){
        return formularioDAO;
    }
    
    
}
