/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import modelo.Formulario;

/**
 *
 * @author Windows
 */
public class FormularioDAO implements IFormularioDAO{
    public void insertar(Formulario fm) throws Exception {
        try {
            EntityManagerHelper.getEntityManager().persist(fm);
        } catch (RuntimeException e) {
            throw e;
        }
    }
}
