/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.util.List;
import modelo.Usuario;

/**
 *
 * @author Windows
 */
public interface IUsuarioDAO {
    public void insertar(Usuario usuario) throws Exception;
    public Usuario consultarPorDocumento(Long numDocumento) throws Exception;
    public List<Usuario> consultar() throws Exception; 
}
