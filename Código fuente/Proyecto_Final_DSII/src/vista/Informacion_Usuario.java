/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.LimitadorCaracteres;
import controlador.RegistroControlador;
import javax.swing.JOptionPane;
import modelo.RegistroIngreso;
import modelo.RegistroUsuario;

/**
 *
 * @author Windows
 */
public class Informacion_Usuario extends java.awt.Dialog {

    /**
     * Creates new form Informacion_Usuario
     */
    public long numDocVig;
    public Informacion_Usuario(java.awt.Frame parent, boolean modal, Long numeroDocVig) {
        super(parent, modal);
        this.numDocVig=numeroDocVig;
        initComponents();
        this.setLocationRelativeTo(null);
        
        txtNDocu.setDocument(new LimitadorCaracteres(txtNDocu, 10, 0));
        txtTiDocu.setDocument(new LimitadorCaracteres(txtTiDocu, 50, 1));
        txtNombre.setDocument(new LimitadorCaracteres(txtNombre, 50, 1));
        txtApe.setDocument(new LimitadorCaracteres(txtApe, 50, 1));
        txtCargo.setDocument(new LimitadorCaracteres(txtCargo, 50, 1));
        txtCiud.setDocument(new LimitadorCaracteres(txtCiud, 50, 1));
        txtDire.setDocument(new LimitadorCaracteres(txtDire, 50, 2));
        txtTelefono.setDocument(new LimitadorCaracteres(txtTelefono, 10, 0));
        txtProg.setDocument(new LimitadorCaracteres(txtProg, 10, 0));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtApe = new javax.swing.JTextField();
        txtTiDocu = new javax.swing.JTextField();
        txtNDocu = new javax.swing.JTextField();
        txtCargo = new javax.swing.JTextField();
        txtProg = new javax.swing.JTextField();
        txtCiud = new javax.swing.JTextField();
        txtDire = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        btnGuar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        txtTelefono = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setTitle("Registrar Usuario");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Century Gothic", 1, 36)); // NOI18N
        jLabel2.setText("Registro de Usuario");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 30, -1, -1));

        jLabel3.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel3.setText("Apellido:");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 250, -1, -1));

        jLabel4.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel4.setText("Nombre:");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 210, -1, -1));

        jLabel5.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel5.setText("Tipo de Documento:");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 290, -1, -1));

        jLabel6.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel6.setText("Dirección:");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 280, -1, -1));

        jLabel7.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel7.setText("Nro Documento:");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, -1, -1));

        jLabel8.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel8.setText("Cargo:");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 330, -1, -1));

        jLabel10.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel10.setText("Ciudad:");
        add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 240, -1, -1));

        jLabel11.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel11.setText("Programa:");
        add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 160, -1, -1));

        txtApe.setEnabled(false);
        add(txtApe, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 250, 120, -1));

        txtTiDocu.setEnabled(false);
        add(txtTiDocu, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 290, 120, -1));
        add(txtNDocu, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 170, 120, -1));

        txtCargo.setEnabled(false);
        add(txtCargo, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 330, 120, -1));

        txtProg.setEnabled(false);
        add(txtProg, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 160, 140, -1));

        txtCiud.setEnabled(false);
        add(txtCiud, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 250, 140, -1));

        txtDire.setEnabled(false);
        add(txtDire, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 290, 140, -1));

        txtNombre.setEnabled(false);
        add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 210, 120, -1));

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/usua.png"))); // NOI18N
        add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 70, -1, -1));
        add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 680, 10));

        btnGuar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/save.png"))); // NOI18N
        btnGuar.setBorder(null);
        btnGuar.setBorderPainted(false);
        btnGuar.setContentAreaFilled(false);
        btnGuar.setFocusPainted(false);
        btnGuar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuarActionPerformed(evt);
            }
        });
        add(btnGuar, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 360, -1, -1));

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/buscar.png"))); // NOI18N
        btnBuscar.setBorder(null);
        btnBuscar.setBorderPainted(false);
        btnBuscar.setContentAreaFilled(false);
        btnBuscar.setFocusPainted(false);
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });
        add(btnBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 150, -1, -1));

        jLabel14.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel14.setText("Telefono");
        jLabel14.setToolTipText("");
        add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 200, -1, -1));

        txtTelefono.setEnabled(false);
        add(txtTelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 200, 140, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/fondo1.jpg"))); // NOI18N
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 720, 420));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        setVisible(false);
        dispose();
    }//GEN-LAST:event_closeDialog

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        RegistroUsuario rg = new RegistroUsuario();
        
        if(!txtNDocu.getText().isEmpty()){
            rg.setNumDocumento(Long.parseLong(txtNDocu.getText()));
        }
        rg.setNumDocVig(numDocVig);
        
        RegistroControlador registroController = new RegistroControlador();
        try {
            
            if(registroController.encontradoBD(rg)){
                txtNDocu.setEnabled(true);
                txtNDocu.setText("");
                
            }else{
                txtNDocu.setEnabled(true);
                txtTelefono.setEnabled(true);
                txtNombre.setEnabled(true);
                txtApe.setEnabled(true);
                txtTiDocu.setEnabled(true);
                txtCargo.setEnabled(true);
                txtProg.setEnabled(true);
                txtCiud.setEnabled(true);
                txtDire.setEnabled(true);
            }
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
        
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnGuarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuarActionPerformed
        RegistroUsuario rg = new RegistroUsuario();
        rg.setNumDocumento(Long.parseLong(txtNDocu.getText()));
        rg.setApellido(txtApe.getText());
        rg.setNombre(txtNombre.getText());
        rg.setTipoDocumento(txtTiDocu.getText());
        rg.setTelefono(Long.parseLong(txtTelefono.getText()));
        rg.setCargo(txtCargo.getText());
        rg.setPrograma(Integer.parseInt(txtProg.getText()));
        rg.setCiudad(txtCiud.getText());
        rg.setDireccion(txtDire.getText());
        rg.setNumDocVig(numDocVig);
        
        RegistroControlador registroController = new RegistroControlador();
        try {
            registroController.enviar(rg);
            txtNDocu.setText("");
            txtTelefono.setEnabled(false);
            txtNombre.setEnabled(false);
            txtApe.setEnabled(false);
            txtTiDocu.setEnabled(false);
            txtCargo.setEnabled(false);
            txtProg.setEnabled(false);
            txtCiud.setEnabled(false);
            txtDire.setEnabled(false);
            
            txtTelefono.setText("");
            txtNombre.setText("");
            txtApe.setText("");
            txtTiDocu.setText("");
            txtCargo.setText("");
            txtProg.setText("");
            txtCiud.setText("");
            txtDire.setText("");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_btnGuarActionPerformed

    /**
     * @param args the command line arguments
     */
    


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnGuar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JSeparator jSeparator1;
    public javax.swing.JTextField txtApe;
    public javax.swing.JTextField txtCargo;
    public javax.swing.JTextField txtCiud;
    public javax.swing.JTextField txtDire;
    public javax.swing.JTextField txtNDocu;
    public javax.swing.JTextField txtNombre;
    public javax.swing.JTextField txtProg;
    public javax.swing.JTextField txtTelefono;
    public javax.swing.JTextField txtTiDocu;
    // End of variables declaration//GEN-END:variables
}
