/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Windows
 */
@Entity
@Table(name = "formulario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Formulario.findAll", query = "SELECT f FROM Formulario f")
    , @NamedQuery(name = "Formulario.findById", query = "SELECT f FROM Formulario f WHERE f.id = :id")
    , @NamedQuery(name = "Formulario.findByEnfermedad", query = "SELECT f FROM Formulario f WHERE f.enfermedad = :enfermedad")
    , @NamedQuery(name = "Formulario.findBySitiosFrecuentados", query = "SELECT f FROM Formulario f WHERE f.sitiosFrecuentados = :sitiosFrecuentados")
    , @NamedQuery(name = "Formulario.findByCantidadMenores", query = "SELECT f FROM Formulario f WHERE f.cantidadMenores = :cantidadMenores")
    , @NamedQuery(name = "Formulario.findByCantidadMayores", query = "SELECT f FROM Formulario f WHERE f.cantidadMayores = :cantidadMayores")})
public class Formulario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "enfermedad")
    private String enfermedad;
    @Basic(optional = false)
    @Column(name = "sitios_frecuentados")
    private String sitiosFrecuentados;
    @Basic(optional = false)
    @Column(name = "cantidad_menores")
    private int cantidadMenores;
    @Basic(optional = false)
    @Column(name = "cantidad_mayores")
    private int cantidadMayores;
    @JoinColumn(name = "numero_documento_usuario", referencedColumnName = "numero_documento")
    @ManyToOne(optional = false)
    private Usuario numeroDocumentoUsuario;
    @JoinColumn(name = "numero_documento_vigilante", referencedColumnName = "numero_documento")
    @ManyToOne(optional = false)
    private Vigilante numeroDocumentoVigilante;

    public Formulario() {
    }

    public Formulario(Integer id) {
        this.id = id;
    }

    public Formulario(Integer id, String enfermedad, String sitiosFrecuentados, int cantidadMenores, int cantidadMayores) {
        this.id = id;
        this.enfermedad = enfermedad;
        this.sitiosFrecuentados = sitiosFrecuentados;
        this.cantidadMenores = cantidadMenores;
        this.cantidadMayores = cantidadMayores;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEnfermedad() {
        return enfermedad;
    }

    public void setEnfermedad(String enfermedad) {
        this.enfermedad = enfermedad;
    }

    public String getSitiosFrecuentados() {
        return sitiosFrecuentados;
    }

    public void setSitiosFrecuentados(String sitiosFrecuentados) {
        this.sitiosFrecuentados = sitiosFrecuentados;
    }

    public int getCantidadMenores() {
        return cantidadMenores;
    }

    public void setCantidadMenores(int cantidadMenores) {
        this.cantidadMenores = cantidadMenores;
    }

    public int getCantidadMayores() {
        return cantidadMayores;
    }

    public void setCantidadMayores(int cantidadMayores) {
        this.cantidadMayores = cantidadMayores;
    }

    public Usuario getNumeroDocumentoUsuario() {
        return numeroDocumentoUsuario;
    }

    public void setNumeroDocumentoUsuario(Usuario numeroDocumentoUsuario) {
        this.numeroDocumentoUsuario = numeroDocumentoUsuario;
    }

    public Vigilante getNumeroDocumentoVigilante() {
        return numeroDocumentoVigilante;
    }

    public void setNumeroDocumentoVigilante(Vigilante numeroDocumentoVigilante) {
        this.numeroDocumentoVigilante = numeroDocumentoVigilante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Formulario)) {
            return false;
        }
        Formulario other = (Formulario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Formulario[ id=" + id + " ]";
    }
    
}
