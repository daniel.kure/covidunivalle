/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Windows
 */
public class RegistroFormulario {
    
    String enfermedad,sitiosFrecuentados;
    long numDocUsuario,numDocVigilante;
    int cantidadMayores,cantidadMenores;

    public String getEnfermedad() {
        return enfermedad;
    }

    public void setEnfermedad(String enfermedad) {
        this.enfermedad = enfermedad;
    }

    public String getSitiosFrecuentados() {
        return sitiosFrecuentados;
    }

    public void setSitiosFrecuentados(String sitiosFrecuentados) {
        this.sitiosFrecuentados = sitiosFrecuentados;
    }

    public long getNumDocUsuario() {
        return numDocUsuario;
    }

    public void setNumDocUsuario(long numDocUsuario) {
        this.numDocUsuario = numDocUsuario;
    }

    public long getNumDocVigilante() {
        return numDocVigilante;
    }

    public void setNumDocVigilante(long numDocVigilante) {
        this.numDocVigilante = numDocVigilante;
    }

    public int getCantidadMayores() {
        return cantidadMayores;
    }

    public void setCantidadMayores(int cantidadMayores) {
        this.cantidadMayores = cantidadMayores;
    }

    public int getCantidadMenores() {
        return cantidadMenores;
    }

    public void setCantidadMenores(int cantidadMenores) {
        this.cantidadMenores = cantidadMenores;
    }
    
    public RegistroFormulario() {
    }
    
}
