/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Windows
 */
@Entity
@Table(name = "vigilante")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vigilante.findAll", query = "SELECT v FROM Vigilante v")
    , @NamedQuery(name = "Vigilante.findByNumeroDocumento", query = "SELECT v FROM Vigilante v WHERE v.numeroDocumento = :numeroDocumento")
    , @NamedQuery(name = "Vigilante.findByTipoDocumento", query = "SELECT v FROM Vigilante v WHERE v.tipoDocumento = :tipoDocumento")
    , @NamedQuery(name = "Vigilante.findByContrasena", query = "SELECT v FROM Vigilante v WHERE v.contrasena = :contrasena")
    , @NamedQuery(name = "Vigilante.findByNombre", query = "SELECT v FROM Vigilante v WHERE v.nombre = :nombre")
    , @NamedQuery(name = "Vigilante.findByApellido", query = "SELECT v FROM Vigilante v WHERE v.apellido = :apellido")
    , @NamedQuery(name = "Vigilante.findByDireccion", query = "SELECT v FROM Vigilante v WHERE v.direccion = :direccion")
    , @NamedQuery(name = "Vigilante.findByTelefono", query = "SELECT v FROM Vigilante v WHERE v.telefono = :telefono")})
public class Vigilante implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "numero_documento")
    private Long numeroDocumento;
    @Basic(optional = false)
    @Column(name = "tipo_documento")
    private String tipoDocumento;
    @Basic(optional = false)
    @Column(name = "contrasena")
    private String contrasena;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "apellido")
    private String apellido;
    @Basic(optional = false)
    @Column(name = "direccion")
    private String direccion;
    @Column(name = "telefono")
    private Integer telefono;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "numeroDocumentoVigilante")
    private Collection<Formulario> formularioCollection;

    public Vigilante() {
    }

    public Vigilante(Long numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public Vigilante(Long numeroDocumento, String tipoDocumento, String contrasena, String nombre, String apellido, String direccion) {
        this.numeroDocumento = numeroDocumento;
        this.tipoDocumento = tipoDocumento;
        this.contrasena = contrasena;
        this.nombre = nombre;
        this.apellido = apellido;
        this.direccion = direccion;
    }

    public Long getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(Long numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    @XmlTransient
    public Collection<Formulario> getFormularioCollection() {
        return formularioCollection;
    }

    public void setFormularioCollection(Collection<Formulario> formularioCollection) {
        this.formularioCollection = formularioCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numeroDocumento != null ? numeroDocumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vigilante)) {
            return false;
        }
        Vigilante other = (Vigilante) object;
        if ((this.numeroDocumento == null && other.numeroDocumento != null) || (this.numeroDocumento != null && !this.numeroDocumento.equals(other.numeroDocumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Vigilante[ numeroDocumento=" + numeroDocumento + " ]";
    }
    
}
