/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Windows
 */
public class RegistroIngreso {
    
    private float temperatura;
    private String hora, fecha;
    private long numDocumento, numDocVig;

    public long getNumDocVig() {
        return numDocVig;
    }

    public void setNumDocVig(long numDocVig) {
        this.numDocVig = numDocVig;
    }
    
    
    public RegistroIngreso() {
    }

    public long getNumDocumento() {
        return numDocumento;
    }

    public void setNumDocumento(long numDocumento) {
        this.numDocumento = numDocumento;
    }
 
    public float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(float temperatura) {
        this.temperatura = temperatura;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    
}
